# api-docs
This documentation describes all API resources that are externally
accessible. Certain endpoints, such as health checks, cannot be
reached from outside of the cluster. Thus, they are not listed here.