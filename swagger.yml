swagger: '2.0'
info:
  version: 1
  title: BotBin API
  description: |
    This API provides access to BotBin features in the same way that the web-based client
    does. In fact, it is the same--just with less visuals and more headaches. Have fun!
    
    # Errors
    The API will respond to invalid requests with an HTTP status code and error message
    that details what went wrong. Codes are consistent with their common interpretations,
    with this documentation providing insight on the specifics.
    
    Error messages will be of the form:
    ```
    {
      "message": "a concise explanation"
    }
    ```
    Messages are primarily meant for diagnostic use; thus, it is not recommended to hard code their values.

    # Rate Limits
    Rate limits are in place to ensure that most users receive a pleasant experience.
    Most resources will share the same limit settings. However, some will use their own
    set of rules. Custom rules are common in the few places where tokens are not required or
    where actions require significant computation to complete. In all cases, the response
    headers will indicate the limit configuration for the type of request that was provided.
    Those headers will have the following shape:
    |          Key          |                         Description                        |
    |:---------------------:|:----------------------------------------------------------:|
    | X-RateLimit-Limit     | Allowed requests per reset period                          |
    | X-RateLimit-Remaining | Remaining requests in period until denial                  |
    | X-RateLimit-Reset     | Timestamp in Unix seconds indicating next reset point      |

  contact:
    name: BotBin
    email: contact@botbin.io
host: 'api.botbin.io'
basePath: /v1
tags:
  - name: users
    description: User accounts allow the service to persist user data between requests.
  - name: tokens
    description: Tokens grant access to protected API resources.
  - name: modules
    description: Modules are versioned functions that a bot invokes in response to some event.
schemes:
  - https
consumes:
  - application/json
produces:
  - application/json
securitySchemes:
  Bearer:
    type: http
    scheme: bearer
    bearerFormat: JWT

paths:
  /users:
    post:
      tags:
        - users
      summary: Create a new account.
      description: This endpoint permits account creation. Each account must
        be tied to a unique Discord account.
      parameters:
        - name: code
          in: path
          required: true
          description: A Discord OAuth2 code
          type: string
        - name: username
          in: path
          required: true
          description: A unique username to be associated with the account
          type: string
      responses:
        '201':
          description: Account created
          schema:
            $ref: '#/definitions/NewAccountResponse'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'

  /tokens:
    post:
      tags:
        - tokens
      summary: Acquire a new token.
      description: This endpoint handles creation of refresh and access tokens.
        Refresh tokens are created in exchange for a Discord OAuth2 code. They
        expire six months after their last use. Access tokens are created in
        exchange for a refresh token made by this service. They expire seven
        days after they are created.
      parameters:
        - name: code
          in: path
          description: A Discord OAuth2 code. If this parameter is given, a refresh token
            and access token will be generated and returned.
          type: string
        - name: refresh_token
          in: path
          description: A refresh token previously granted by this service.
            If this parameter is given, a corresponding access token will be generated
            and returned.
          type: string
      responses:
        '201':
          description: Token created
          schema:
            $ref: '#/definitions/TokenResponse'
        '400':
          description: Missing parameter value
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Invalid Discord access token provided
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  /tokens/revoked:
    post:
      tags:
        - tokens
      summary: Revoke a refresh token.
      description: A refresh token can be revoked in order to invalidate the
        access tokens it has created and prevent it from being used to make more.
      parameters:
        - name: refresh_token
          in: path
          description: The refresh token to revoke
          required: true
          type: string
      responses:
        '200':
          description: Token revoked
        '400':
          description: Missing parameter value
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'

  /modules:
    post:
      tags:
        - modules
      summary: Create a new module.
      description: This request will queue a new tagged module for creation.
        Modules cannot be changed after creation, so tags are used to express
        a history of versioning for a module with a given name. Thus, tags must
        be unique, but names can be reused for subsequent tags.


        Note that this only queues a module to be created. That process may fail
        if the code does not follow conventions of the target language. Final
        build results are sent through the notification system when the process
        completes.
      parameters:
        - name: module
          in: body
          required: true
          description: The module definition
          schema:
            $ref: '#/definitions/NewModule'
      responses:
        '200':
          description: Module queued for creation
        '400':
          description: Bad module format
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    get:
      tags:
        - modules
      summary: Get modules that match a query.
      description: This is the search endpoint for all modules. It provides
        a way to search based on fields, such as name or author, and it
        also supports generic queries.


        Results are paginated. This means the number of results can be specified
        and a cursor can be used to walk through them across multiple requests.
      parameters:
        - name: from
          in: path
          description: The cursor at which results should start. This value
            defaults to 0 if not provided.
          type: integer
          format: int64
        - name: limit
          in: path
          description: The maximum number of results to return. This value must
            be in the range [0, 20] and will default to 20 if not provided.
          type: integer
          format: int64
        - name: q
          in: path
          description: A generic query. If provided, specific search fields are
            ignored.
          type: string
        - name: author
          in: path
          description: The user whose modules are being searched
          type: string
        - name: name
          in: path
          description: The name common to all modules being searched
          type: string
        - name: tag
          in: path
          description: The tag common to all modules being searched
          type: string
      responses:
        '200':
          description: Search completed successfully
          schema:
            type: array
            items:
              $ref: '#/definitions/Module'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'

definitions:
  Error:
    description: A description of what caused a request to fail
    type: object
    properties:
      message:
        description: The error message
        type: string
  NewAccountResponse:
    description: A token that grants access to the service
    type: object
    properties:
      refreshToken:
        description: A refresh token that can be exchanged for access
          tokens
        type: string
      accessToken:
        description: An access token that grants access to the service
        type: string
      expiresAt:
        description: The moment that the access token will expire. This
          is a timestamp in Unix seconds
        type: integer
        format: int64
  TokenResponse:
    description: A token that grants access to the service
    type: object
    properties:
      refreshToken:
        description: A refresh token that can be exchanged for access
          tokens. This value is only present if explicitly requested.
        type: string
      accessToken:
        description: An access token that grants access to the service
        type: string
      expiresAt:
        description: The moment that the access token will expire. This
          is a timestamp in Unix seconds
        type: integer
        format: int64
  Language:
    description: A supported programming language
    type: string
    enum: &LANGUAGE
      - js
  NewModule:
    description: The definition of a new module
    type: object
    properties:
      name:
        description: A title given to the module that should indicate what
          the various tagged versions of it will do. Name must be unique
          within the scope of a user's collection.
        type: string
      tag:
        description: A short string that identifies a specific version of
          a module
        type: string
      description:
        description: An optional description of what the module will do
        type: string
      language:
        description: The language in which the module is written
        type: string
        enum: *LANGUAGE
      code:
        description: The source code that should be executed when the
          module is invoked. This must be base64-encoded.
        type: string
        format: byte
  Module:
    description: The definition of an existing module
    type: object
    properties:
      id:
        description: The unique id of the module
        type: integer
        format: int64
      createdAt:
        description: A timestamp in RFC 3339 format that indicates when the
          module was created
        type: string
        format: date-time
      fqn:
        description: A unique string to identify the module
        type: string
      author:
        description: The username of the module creator
        type: string
      name:
        description: A title given to the module that should indicate what
          the various tagged versions of it will do
        type: string
      tag:
        description: A short string that identifies a specific version of
          a module
        type: string
      description:
        description: An optional description of what the module will do
        type: string
      language:
        description: The language in which the module is written
        type: string
        enum: *LANGUAGE
      code:
        description: The source code that should be executed when the
          module is invoked. This is base64-encoded.
        type: string
        format: byte
